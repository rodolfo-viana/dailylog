<p align="center"><img src="http://simpleicon.com/wp-content/uploads/Calendar-1.png" alt="" width="100" /></p>

# Daily Log

Projects, scripts, notebooks, backup files, studies log and works in general

# About the author

![](https://i.imgur.com/MJQNRLk.jpg)

Data scientist. Lead developer at Nove de Julho, a project created to audit open data related to state congressmen. Contributing elsewhere.

Website: [`rodolfoviana.com.br`](https://rodolfoviana.com.br/)<br>
Email: [`eu@rodolfoviana.com.br`](mailto:eu@rodolfoviana.com.br)<br>
LinkedIn: [`linkedin.com/in/rodolfoviana`](https://linkedin.com/in/rodolfoviana)
